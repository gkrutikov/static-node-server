const fs = require('fs');
const http = require('http');
const { promisify } = require('util');
const readdir = promisify(fs.readdir);
const readFile = promisify(fs.readFile);

const server = http.createServer(async (req, res) => {
  try {
    const items = await readdir('./' + req.url);
    if (items.length) {
      items.map(async (item) => {
        res.write(`<a href=${req.url === '/' ? req.url + item : req.url + '/' + item}>${item}</a> </br>`);
      });
    } else {
      res.end('Folder is empty')
    }
  } catch (err) {
    if (err.code === "ENOENT") {
      res.writeHead(404);
      res.end(`There\'s no folder or file called ${req.url.slice(1)}`);
    }
    if (err.code === 'ENOTDIR') {
      const file = await readFile(__dirname + req.url, 'utf-8');
      res.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
      res.end(file);
    }
  }
});

server.listen(8080);